package com.example.gif_lib.model;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private int id;

    private String name;

    private static List<Category> allCategories = new ArrayList<>();

    static {
        Category category1 = new Category(0,"Android");
        Category category2 = new Category(1,"Funny");
        Category category3 = new Category(2,"Programming");

        allCategories.add(category1);
        allCategories.add(category2);
        allCategories.add(category3);
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }



    public static List<Category> getAllCategories() {
        return allCategories;
    }

    public static List<String> getGifsByCategory(Category category){
        List<String> myGifs = new ArrayList<>();
        for (Gif gif : Gif.getAllGifs()) {
            if (gif.getCategory().getId() == category.getId()) {
                String result = gif.getGifName(gif.getName());
                myGifs.add(result);
            }

        }
        return myGifs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
