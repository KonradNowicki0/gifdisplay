package com.example.gif_lib.dao;

import com.example.gif_lib.model.Gif;


import java.util.List;

public interface GifDao {

    String getGifName(String name); // it should return address + name of file

    List <String> getUrl(List <Gif> nameList);

    List<String> findFavourites(List <Gif> nameList);

}